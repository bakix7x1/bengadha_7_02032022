let selectedIngredients = []
let selectedAppliences = []
let selectedUstensils = []
let enteredText = ''




// filte par dropdown



let allRecipesFilter = []

function filterByImgredients (ingredientName, recipes){
    selectedIngredients.push (ingredientName)
    const result = []
    // for pour parcourire les recettes
    for (const recipe of recipes) {
    
        // permet de recuperer tout les ingredients
        const ingredients = recipe.ingredients
        for (const ingredient of ingredients ) {
            
            // permet de verifier si l'ingredientName = a un nom d'ingredients
            if (ingredientName === ingredient.ingredient){
                result.push (recipe)
            }
            
        }
        
    }
    return result
}




function filterByAppliance (applianceName, recipes){
    selectedAppliences.push(applianceName)
    const result = []
    // for pour parcourire les recettes
    for (const recipe of recipes) {
        
        // permet de recuperer tout les appliances
        const appliance = recipe.appliance
            
            // permet de verifier si l'applianceName = a un nom d'appliances
            if (applianceName === appliance){
                result.push (recipe)
            }
            
        
        
    }
    return result
}

function filterByUstensils (ustensilName, recipes){
    selectedUstensils.push (ustensilName)
    const result = []
    // for pour parcourire les recettes
    for (const recipe of recipes) {
        // permet de recuperer tout les ustensils
        const ustensils = recipe.ustensils
        for ( const ustensil of ustensils) {
           
            // permet de verifier si l'ustensilName = a un nom d'ustensils
            if (ustensilName === ustensil){
                result.push (recipe)
            }
            
        }
        
    }
    return result
}


//filtre pour search

function filterBySearchBar(texte, recipes) {
    enteredText = texte
    let result = []
    

    if (!texte) {
        result = resetSearchTag()
    }else{
        
        for (const recipe of recipes) {
        
        const nornalizeText = toNormalForm(texte)
        if (verifName(nornalizeText, recipe)|| verifDescription(nornalizeText, recipe)|| verifSearchIngredient(nornalizeText,recipe)) {
            result.push(recipe)
        }
    }}
    return result
}
// si le texte on le trouve dans nom de la recette si oui true si non false

function verifName(texte, recipe) {
    const recipeName = toNormalForm(recipe.name) 
    if (recipeName.includes(texte)) {
        return true
    }
    else{
        return false
    }
    
}
// si le texte on le trouve dans la description de la recette si oui true si non false
function verifDescription(texte, recipe) {
    const recipeDescription = toNormalForm(recipe.description)
    if (recipeDescription.includes(texte)) {
        return true
    }
    else{
        return false
    }
    
}

function verifSearchIngredient(texte, recipe) {
    for (const ingredient of recipe.ingredients){
        //recuper l'ingredient
        
        //recuper le nom
        const ingredientName = toNormalForm(ingredient.ingredient)
        
        //on verifie si le nom est inclu dans selectedIngredients
        if (ingredientName.includes(texte)) {
            return true
            
        }
    }
    return false

    
}

// suprimer un ingredien parmis les ingredient selectioner
function removeIngredient(ingredientSup) {

    selectedIngredients = selectedIngredients.filter(ingredient=>ingredient!=ingredientSup)
    return resetSearch()
} 


// il fais la recherche en fonction de tout les element selectionner 
function resetSearch() {
    
    let result = resetSearchTag()


    if (enteredText.length>=3) {
        result = filterBySearchBar(enteredText, result)
    }
    
    
    return result
}

function resetSearchTag() {
    const result = []
    for (const recipe of allRecipes){
        
        const ingredients = recipe.ingredients
        const appliance = recipe.appliance
        const ustensils = recipe.ustensils
        if (verifyIngredients(ingredients)&&verifyAppliances(appliance)&&verifyUstensils(ustensils)) {
            result.push (recipe)
        }
    }
    return result
}

//elle dois verifier si parmit les ingediens passer en parametre il y a au moins 1 ingredient parmis les ingredient selec
function verifyIngredients (recipeIngredients){
    if (selectedIngredients.length==0) {
        return true
    }
    for (const recipeIngredient of recipeIngredients){
        //recuper l'ingredient
      
        
        //recuper le nom
        const ingredientName = recipeIngredient.ingredient
        
        //on verifie si le nom est inclu dans selectedIngredients
        if (selectedIngredients.includes(ingredientName)) {
            return true
            
        }
    }
}


// suprimer un ustensil parmis les ustensil selectioner
function removeUstensil(ustensilSup) {

    selectedUstensils = selectedUstensils.filter(ustensil=>ustensil!=ustensilSup)
    return resetSearch()
} 



//elle dois verifier si parmit les ingediens passer en parametre il y a au moins 1 ustensil parmis les ustensil selec
function verifyUstensils (recipeUstensils){
    if (selectedUstensils.length==0) {
        return true
    }
    for (const recipeUstensil of recipeUstensils){
        //recuper l'ustensi
       
        
        //on verifie si le nom est inclu dans selectedUstensils
        if (selectedUstensils.includes(recipeUstensil)) {
            return true
            
        }
    }
}

// suprimer un appliance parmis les Appliance selectioner
function removeAppliance(applianceSup) {

    selectedAppliences = selectedAppliences.filter(appliance=>appliance!=applianceSup)
    return resetSearch()
} 

//elle dois verifier si parmit les ingediens passer en parametre il y a au moins 1 Appliance parmis les Applianceselec
function verifyAppliances (recipeAppliances){
    if (selectedAppliences.length==0) {
        return true
    }
    return selectedAppliences.includes(recipeAppliances)
    
}