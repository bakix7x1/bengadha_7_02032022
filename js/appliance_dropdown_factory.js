
let appliances = []
const applianceChoose = []
//let recipesDisplay = []
const applianceDropdown = document.querySelector('#appliance-dropdown')

function applianceDropdownFactory(allAppliances, recipes) {
    appliances  = [...allAppliances].filter(appliance=>!applianceChoose.includes(appliance))

    appliances  = [...allAppliances]


    //recipesDisplay = [...recipes]
    
    console.log(allAppliances)

    displayDropDownAppliance(allAppliances)


    initSearchDropdownAppliance()
    
}

function initSearchDropdownAppliance() {


    const searchAppliance = document.querySelector('#search-appliance')



    searchAppliance.addEventListener('input', (e)=>{

        resetDropdownAppliance()

        const text = toNormalForm(e.target.value) 
        console.log(appliances);
    
        const filterAppliances = appliances.filter((appliance)=>toNormalForm(appliance).includes(text))
        
        displayDropDownAppliance(filterAppliances)

    })


    
}

function resetDropdownAppliance() {
    const allAppliancesItem = document.querySelectorAll('.appliance-item')

    for (const item of allAppliancesItem) {
        item.remove()
        
    }
    
}

function displayDropDownAppliance(applianceDisplay) {
    resetDropdownAppliance() 
    for (const appliance of applianceDisplay) {

        const li = document.createElement('li')
    const a = document.createElement('a')
    a.setAttribute('class', 'dropdown-item item-appliance')
    li.setAttribute('class', 'appliance-item')
    li.appendChild(a)
    a.textContent= appliance
    applianceDropdown.appendChild(li)
    li.addEventListener('click', (e)=>{
        executSearchAppliance(appliance)
        const span = document.createElement('span')
        const i = document.createElement ('i')
        const div = document.querySelector ('.select-items')
        span.setAttribute('class', 'select-items-appliance')
        span.textContent= appliance
        i.setAttribute('class', 'far fa-times-circle')
        span.appendChild(i)
        div.appendChild(span)
        i.addEventListener('click', (e)=> {span.remove(); removeAppliancetSelec(appliance)})
    })
        
    }
    
}


function executSearchAppliance(appliance) {
   console.log(appliance);
    const filterRecipes = filterByAppliance(appliance , recipesDisplay)

    displaySortedRecipes(filterRecipes)

    
}

function removeAppliancetSelec(appliance) {
    const filterRecipes = removeAppliance(appliance)
    displaySortedRecipes(filterRecipes)
}