let allRecipes = []
// CREATION DOM ELEMENTS
// FACTORY METHOD : type + attributes + nodes
const elmtFactory = (type, attribute, ...children) => {
    let elmt = document.createElement(type);
  
    for (key in attribute) {
      elmt.setAttribute(key, attribute[key]);
    }
  
    children.forEach((child) => {
      if (typeof child === 'string')
        elmt.appendChild(document.createTextNode(child));
      else elmt.appendChild(child);
    });
  
    return elmt;
  };
  
  
  // elle permet d'afficher certaine recette de demander
  function displaySortedRecipes(recipes) {
    const section =document.querySelector('#recipes')
    section.innerHTML= ''
    
    let ingredients = []
    let ustensils = []
    let appliance = []


    if (recipes.length==0) {
      const p = document.createElement('p')
      p.textContent='Aucune recette retrouver rechercher avec coco, tomate ou encore carote .'
      section.appendChild(p)
    }else{
      console.log(recipes)
    //allRecipes = recipes
    

    for (const recipe of recipes ) {
      setRecipe(recipe);
      ingredients.push(...recipe.ingredients)
      ustensils.push(...recipe.ustensils)
      appliance.push(recipe.appliance)
    }
    ingredients=ingredients.map(ingredient=>ingredient.ingredient)
    }
    
    


    ingredientDropdownFactory(new Set(ingredients),recipes)
  
    ustensilDropdownFactory(new Set(ustensils), recipes)

    applianceDropdownFactory(new Set(appliance), recipes)

    refreshRecipies (recipes)
  }
//afficher les cartes de recette au demagare de ap
  function initRecipes(recipes) {
    allRecipes = recipes
    displaySortedRecipes(recipes)

    
  }



