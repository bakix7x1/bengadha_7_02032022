
let ingredients = []
let recipesDisplay = []
const ingredientChoose = []

const ingredientDropdown = document.querySelector('#ingredient-dropdown')

function ingredientDropdownFactory(allIngredients, recipes) {
    ingredients  = [...allIngredients].filter(ingredient=>!ingredientChoose.includes(ingredient))

    recipesDisplay = [...recipes]
    
    console.log(allIngredients)

    displayDropDownIngredient(ingredients)


    initSearchDropdownIngredient()
    
}

function initSearchDropdownIngredient() {


    const searchIngredient = document.querySelector('#search-ingredient')



    searchIngredient.addEventListener('input', (e)=>{

        resetDropdownIngredient()

        const text = toNormalForm(e.target.value )
        console.log(ingredients);
    
        const filterIngredients = ingredients.filter((ingredient)=>toNormalForm(ingredient).includes(text))
        
        displayDropDownIngredient(filterIngredients)

    })


    
}

function resetDropdownIngredient() {
    const allIngredientsItem = document.querySelectorAll('.ingredient-item')
    

    for (const item of allIngredientsItem) {
        item.remove()
        
    }
    
}

function displayDropDownIngredient(ingredientDisplay) {
    resetDropdownIngredient()
    for (const ingredient of ingredientDisplay) {

        const li = document.createElement('li')
    const a = document.createElement('a')
    a.setAttribute('class', 'dropdown-item item-ingredient' )
    li.setAttribute('class', 'ingredient-item')
    li.appendChild(a)
    a.textContent= ingredient
    ingredientDropdown.appendChild(li)
    
    //evenemt qui se passe
    li.addEventListener('click', (e)=>{
        ingredientChoose.push (ingredient)
        executSearchIngredient(ingredient)
        const span = document.createElement('span')
        const i = document.createElement('i')
        const div= document.querySelector('.select-items')
        span.setAttribute('class', 'select-items-ingredient')
        span.textContent= ingredient
        i.setAttribute('class', 'far fa-times-circle')
        span.appendChild(i)
        div.appendChild(span)
        i.addEventListener('click', (e)=> {span.remove();removeIngredientSelec(ingredient)} )

        
    })
        
    }
    
}




function executSearchIngredient(ingredient) {

    const filterRecipes = filterByImgredients(ingredient , recipesDisplay)

    displaySortedRecipes(filterRecipes)

    
}

function removeIngredientSelec(ingredien) {
    
    const filterRecipes = removeIngredient(ingredien)
    displaySortedRecipes(filterRecipes)
}