fetch('recipes.json')
  .then((response) => response.json())
  .then((data) => {
    const recipes = data.recipes;

    // AFFICHE CARTES RECETTES
    initRecipes(recipes);
  });