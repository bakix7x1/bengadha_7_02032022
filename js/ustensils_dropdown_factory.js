
let ustensils = []
const ustensilChoose = []
//let recipesDisplay = []
const ustensilDropdown = document.querySelector('#ustensil-dropdown')

function ustensilDropdownFactory(allUstensils, recipes) {
    ustensils  = [...allUstensils].filter(ustensil=>!ustensilChoose.includes(ustensil))

    ustensils  = [...allUstensils]

    //recipesDisplay = [...recipes]
    
    console.log(allUstensils)

    displayDropDownUstensil(allUstensils)


    initSearchDropdownUstensil()
    
}

function initSearchDropdownUstensil() {


    const searchUstensil = document.querySelector('#search-ustensil')



    searchUstensil.addEventListener('input', (e)=>{

        resetDropdownUstensil()

        const text = toNormalForm(e.target.value) 
        console.log(ustensils);
    
        const filterUstensils = ustensils.filter((ustensil)=>toNormalForm(ustensil).includes(text))
        
        displayDropDownUstensil(filterUstensils)

    })


    
}

function resetDropdownUstensil() {
    const allUstensilsItem = document.querySelectorAll('.ustensil-item')

    for (const item of allUstensilsItem) {
        item.remove()
        
    }
    
}

function displayDropDownUstensil(ustensilsDisplay) {
    resetDropdownUstensil() 
    for (const ustensils of ustensilsDisplay) {

        const li = document.createElement('li')
    const a = document.createElement('a')
    a.setAttribute('class', 'dropdown-item')
    li.setAttribute('class', 'ustensil-item')
    li.appendChild(a)
    a.textContent= ustensils
    ustensilDropdown.appendChild(li)
    li.addEventListener('click', (e)=>{
        executSearchUstensil(ustensils)
        const span = document.createElement('span')
        const i = document.createElement('i')
        const div= document.querySelector('.select-items')
        span.setAttribute('class', 'select-items-ustensil')
        span.textContent= ustensils
        i.setAttribute('class', 'far fa-times-circle')
        span.appendChild(i)
        div.appendChild(span)
        i.addEventListener('click', (e)=> {span.remove(); removeUstensilSelec(ustensils)})
    })
        
    }
    
}


function executSearchUstensil(ustensil) {

    const filterRecipes = filterByUstensils(ustensil , recipesDisplay)

    displaySortedRecipes(filterRecipes)

    
}

function removeUstensilSelec(ustensil) {
    const filterRecipes = removeUstensil(ustensil)
    displaySortedRecipes(filterRecipes)
}